import {times} from 'lodash';
import {models} from '@kentcdodds/send-money-common';
const mockTransaction = models.transaction.mock;

const transactions = times(500, mockTransaction);

process.stdout.write(JSON.stringify({transactions}));

