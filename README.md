# send-money-api

This is the backend portion of the Send Money implementation by [Kent C. Dodds](https://twitter.com/kentcdodds)

## Setup

See [CONTRIBUTING.md](CONTRIBUTING.md) for information on how to
get set up and contribute to this project

## Assumptions

This project was developed to support the send-money application. There
were a few assumptions that I made while developing this application.

### Codebase scaling

When the first iteration of this project is done, we'll want to continue
developing new features on it and maintaing it in the long run. This is
why we have a strict code coverage standard and linting standard from
the beginning and we enforce this using git hooks with `ghooks`.

We've also organized the code to match the REST API

> Side-note, I could have easily used something like
> [`json-server`](http://npm.im/json-server) and been done with this really quick.
> I decided against this because I didn't think that would demonstrate
> my abilities very well, so I opted for a custom express server

## Credits

This project relies heavily on work from open source libraries. Huge
thank you to them! You can see the libraries used in the
[package.json](package.json).

All of the code in the [`src`](src) directory is original code from
[Kent C. Dodds](https://twitter.com/kentcdodds) (and I'm sure some of it
was modified from various examples and StackOverflow answers from around
the interwebs).

