import app from './app';
const port = process.env.PORT || 8989;
app.listen(port, () => {
  console.log(`server running on port: ${port}`); // eslint-disable-line no-console
});
