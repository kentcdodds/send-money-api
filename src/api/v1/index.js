import express from 'express';
import transactions from './transactions';

const v1 = express();

v1.use('/transactions', transactions);

export default v1;

