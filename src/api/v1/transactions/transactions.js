import express from 'express';
import bodyParser from 'body-parser';
import {models} from '@kentcdodds/send-money-common';
const {transaction: createTransaction} = models;


// yes, this is definitely one of the coolest
// most advanced databases of all time (－‸ლ)
const db = require('../../../data/db.json');
const sortedTransactions = sortTransactions(db.transactions);

// setup the app
const transactions = express();

transactions.use(bodyParser.json());
transactions.get('/', get);
transactions.post('/', post);

export default transactions;


// function declarations

function get(req, res) {
  let {offset, count} = req.query;
  offset = Number.parseInt(offset, 10);
  count = Number.parseInt(count, 10);
  let responseTransactions = sortedTransactions;
  if (Number.isInteger(offset) && Number.isInteger(count)) {
    responseTransactions = responseTransactions.slice(offset, offset + count);
  }
  res.json(responseTransactions);
}

function post(req, res) {
  const transaction = createTransaction(req.body);
  // here's where I'd asynchronously save the transaction
  // to some database
  res.status(201).json(transaction);
}

function sortTransactions(trans) {
  return trans.sort((a, b) => {
    return new Date(b.date) - new Date(a.date);
  });
}

// I realize this may look really crazy, but believe me,
// I've been doing this for a while and it's got a lot of
// pros. Please ask me about this if you're concerned :-)
/* istanbul ignore next */
if (process.env.NODE_ENV === 'test') {
  require('./transactions.test')(transactions);
}
