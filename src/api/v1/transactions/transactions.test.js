import {expect} from 'chai';
import {models} from '@kentcdodds/send-money-common';
import request from 'supertest';
const mockTransaction = models.transaction.mock;

export default tests;

function tests(transactions) {
  describe(`transactions`, () => {
    describe(`get`, () => {
      it(`should send transactions`, done => {
        request(transactions)
          .get('/')
          .expect('Content-Type', /json/)
          .expect(res => {
            expect(res.body, 'res.body is an array').to.be.an('array');
            // 250 is the requirement listed in the instructions :-)
            expect(
              res.body,
              'res.body has minimum length requirement'
            ).to.have.length.above(250);
          })
          .expect(200, done);
      });

      it(`should accept an 'offset' and 'count'`, done => {
        const count = 20;
        // TODO, test offset as well
        request(transactions)
          .get('/')
          .query({offset: 10, count})
          .expect('Content-Type', /json/)
          .expect(res => {
            expect(
              res.body,
              'res.body has length of given count'
            ).to.have.length(count);
          })
          .expect(200, done);
      });
    });

    describe(`post`, () => {
      it(`should respond with the transaction object`, done => {
        const postData = mockTransaction();
        request(transactions)
          .post('/')
          .send(postData)
          .expect(201, postData, done);
      });
    });
  });
}
