import express from 'express';
import cors from 'cors';
import v1 from './v1';

const api = express();

api.use(cors());
api.use('/v1', v1);

export default api;
