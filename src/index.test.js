import chai from 'chai';
import sinonChai from 'sinon-chai';
chai.use(sinonChai);

// must require so chai.use runs first
// and I don't want to create an entire
// file to do just that.
require('./app');

