# Contributing

So you want to contribute! That's terrific! Let's get you set up...

## Set up

1. Install [git](http://git-scm.com/)
2. Install [node and npm](https://docs.npmjs.com/getting-started/installing-node)
3. Clone the repository `$ git clone https://kentcdodds@bitbucket.org/kentcdodds/send-money-api.git`
4. Enter the cloned directory `$ cd send-money-api`
5. Install dependencies `$ npm install`
6. Run tests `$ npm t`

The tests should run and should indicate 100% code coverage. If you're
seeing this then you're all setup! Congratulations!

## Development

Now let's get you changing code!

1. Run `$ npm run test:watch` to have the tests running as you make changes
2. Make changes (tests first if you like)
3. Commit your changes with `$ git commit -am 'Some useful message'`
4. Notice that `eslint` and the tests are run before your commit actually happens :-)
5. If your code passes our coding standard, the tests, and the coverage threshold, you're good to go!
6. If not, please fix your code until it does :-)

## Deployment

Currently, we aren't actually deployed to anywhere, but to "deploy"
locally, you'll want to build the code. It's written in ES6 using babel.
During development, we just have babel transpile our code on the fly.
This isn't really practical for a deployed application though! So that's
why the `start` script for this server runs the `dist` folder. You need
to run the build!

1. Run `$ npm run build`
2. Run `$ npm start`

This will get the server going on the built files. When we actually
deploy this to a real service, we'll need to ensure that the service
will automatically restart the server if it crashes.

## Roadmap

1. Get a real database (mongo, rethink?)
2. Take actual transactions and add them to the database
3. Develop new features as they come!

---

Thanks!!! Let's build something awesome :-D

